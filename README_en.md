# 64-猫的薛定谔

#### Description
TOPIC_ID:64, TEAM_ID:1090721196, TEAM_NAME:猫的薛定谔.

#### Software Architecture
This is a Python based barcode recognition program！

#### Installation

1.  Download program：`git clone https://gitee.com/openeuler2020/team-1090721196.git`
2.  Enter program directory：`cd team-1090721196/Program`
3.  For the first time, it is recommended to use script startup to check the environment dependency：`bash start.sh`
4.  After running normally, in order to reduce unnecessary time, you can run the main program directly：`python3 distinguish.py`


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

