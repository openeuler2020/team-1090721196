#!/bin/bash

distinguish()
{
    python3 distinguish.py
}

check_opencv-python()
{
    if [ `python3 -m pip list opencv-python | wc -l` -ne 0 ];then
    echo  "opencv-python is ok!"
    echo  "starting..."
    distinguish
    else
    echo "try command：python3 -m pip install opencv-python"
    fi
}

check_zbar()
{
    if [ `yum list installed | grep zbar | wc -l` -ne 0 ];then
    echo  "zbar is ok!"
    echo  "checking openccv-python..."
    check_opencv-python
    else
    echo "try command：yum install zbar"
    fi
}

check_python() 
{
    if [ `yum list installed | grep python3 | wc -l` -ne 0 ];then
    echo  "python3 is ok!"
    echo  "checking zbar..."
    check_zbar
    else
    echo "try command：yum install python3"
    fi
}

start()
{
    echo "cheaking dependence..."
    check_python
}

start
