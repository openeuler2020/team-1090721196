import math
from PIL import ImageFont

import cv2
import pyzbar.pyzbar as pyzbar
import numpy as np
import imutils


# 定位并且裁剪
def pos(image, i):
    imgf = image
    #转化为灰度图像
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    #建立图像的梯度幅值
    ddepth = cv2.CV_32F if imutils.is_cv2() else cv2.CV_32F
    gradX = cv2.Sobel(gray, ddepth=ddepth, dx=1, dy=0, ksize=-1)
    gradY = cv2.Sobel(gray, ddepth=ddepth, dx=0, dy=1, ksize=-1)

    #省略掉y方向的梯度幅值
    gradient = cv2.subtract(gradX, gradY)

    #归一化为八位图像
    gradient = cv2.convertScaleAbs(gradient)

    #使用滤波模糊化，使一些噪点消除
    blurred = cv2.blur(gradient, (i, i))

    #得到二值化图像
    (_, thresh) = cv2.threshold(blurred, 235, 255, cv2.THRESH_BINARY)

    #进行形态学闭运算
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 7))
    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    #进行腐蚀膨胀
    closed = cv2.erode(closed, None, iterations=14)
    closed = cv2.dilate(closed, None, iterations=14)

    #对目标轮廓进行查找
    cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    #寻找定位的一个四元组，并且裁剪返回
    if len(cnts) > 0:
        c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]
        rect = cv2.minAreaRect(c)
        box = cv2.boxPoints(rect) if imutils.is_cv2() else cv2.boxPoints(rect)
        box = np.int0(box)
        Xs = [i[0] for i in box]
        Ys = [i[1] for i in box]
        x1 = min(Xs) - 25
        x2 = max(Xs) + 25
        y1 = min(Ys) - 25
        y2 = max(Ys) + 25
        hight = y2 - y1
        width = x2 - x1
        if y1 < 0:
            y1 = 0
        if x1 < 0:
            x1 = 0
        return image[y1:y1 + hight, x1:x1 + width], (x1 - 20, y1 - 20, x1 + width - 20, y1 + hight - 20)
    return imgf, (0, 0, 0, 0)

#倾斜矫正
def fourier_demo(imga):
    # 1、读取文件，灰度化
    img = imga
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # 2、图像延扩
    h, w = img.shape[:2]
    new_h = cv2.getOptimalDFTSize(h)
    new_w = cv2.getOptimalDFTSize(w)
    right = new_w - w
    bottom = new_h - h
    nimg = cv2.copyMakeBorder(gray, 0, bottom, 0, right, borderType=cv2.BORDER_CONSTANT, value=0)

    # 3、执行傅里叶变换，并过得频域图像
    f = np.fft.fft2(nimg)
    fshift = np.fft.fftshift(f)
    magnitude = np.log(np.abs(fshift))

    # 二值化
    magnitude_uint = magnitude.astype(np.uint8)
    ret, thresh = cv2.threshold(magnitude_uint, 11, 255, cv2.THRESH_BINARY)

    # 霍夫直线变换
    lines = cv2.HoughLinesP(thresh, 2, np.pi / 180, 30, minLineLength=40, maxLineGap=100)

    # 创建一个新图像，标注直线
    lineimg = np.ones(nimg.shape, dtype=np.uint8)
    lineimg = lineimg * 255

    piThresh = np.pi / 180
    pi2 = np.pi / 2

    for line in lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(lineimg, (x1, y1), (x2, y2), (0, 255, 0), 2)
        if x2 - x1 == 0:
            continue
        else:
            theta = (y2 - y1) / (x2 - x1)
        if abs(theta) < piThresh or abs(theta - pi2) < piThresh:
            continue

    angle = math.atan(theta)
    angle = angle * (180 / np.pi)
    if abs(angle - 90.0) < 10.0 or abs(angle + 90.0) < 10.0 or abs(angle - 0) < 10.0:
        return imga
    angle = (angle - 90) / (w / h)
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(img, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    rotated = rotate_bound(rotated, 270)
    return rotated

def Barcode(gray):
    texts = pyzbar.decode(gray)
    if not texts:
        angle = barcode_angle(gray)
        if angle < -45:
            angle = -90 - angle
        texts = bar(gray, angle)
    if not texts:
        gray = np.uint8(np.clip((1.1 * gray + 10), 0, 255))
        angle = barcode_angle(gray)
        if angle < -45:
            angle = -90 - angle
        texts = bar(gray, angle)
    return texts


def bar(image, angle):
    gray = image
    bar = rotate_bound(gray, 0 - angle)
    roi = cv2.cvtColor(bar, cv2.COLOR_BGR2RGB)
    texts = pyzbar.decode(roi)
    return texts

def barcode_angle(image):
    gray = image
    ret, binary = cv2.threshold(gray, 220, 255, cv2.THRESH_BINARY_INV)
    kernel = np.ones((8, 8), np.uint8)
    dilation = cv2.dilate(binary, kernel, iterations=1)
    erosion = cv2.erode(dilation, kernel, iterations=1)
    erosion = cv2.erode(erosion, kernel, iterations=1)
    erosion = cv2.erode(erosion, kernel, iterations=1)
    contours, hierarchy = cv2.findContours(
        erosion, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    if len(contours) == 0:
        rect = [0, 0, 0]
    else:
        rect = cv2.minAreaRect(contours[0])
    return rect[2]

def rotate_bound(image, angle):
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    return cv2.warpAffine(image, M, (nW, nH))

if __name__ == '__main__':
    # 读取
    img_url = input("请输入图片的路径")
    while img_url != 0:
        img_data = cv2.imread(img_url)
        img_flu = img_data.copy()
        # 直接识别
        barcodes = pyzbar.decode(img_data)
        if len(barcodes) > 0:
            for barcode in barcodes:
                # 提取条形码的边界框的位置
                # 画出图像中条形码的边界框
                (x, y, w, h) = barcode.rect
                cv2.rectangle(img_data, (x, y), (x + w, y + h), (0, 255, 0), 2)
                # 条形码数据为字节对象，所以如果我们想在输出图像上
                # 画出来，就需要先将它转换成字符串
                barcodeData = barcode.data.decode("utf-8")
                barcodeType = barcode.type
                # 参数（字体，默认大小）
                font = ImageFont.truetype('msyh.ttc', 35)
                # 字体颜色（rgb)
                fillColor = (0, 255, 255)
                # 文字输出位置
                position = (x, y - 10)
                # 输出内容
                str = barcodeData
                # 需要先把输出的中文字符转换成Unicode编码形式(  str.decode("utf-8)   )
                cv2.putText(img_data, str, position, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2)
                # 向终端打印条形码数据和条形码类型
                print("扫描结果==》 类别： {0} 内容： {1}".format(barcodeType, barcodeData))
            cv2.imwrite("result-" + img_url[0:-4] + ".png", img_data, params=None)
        # 如果识别不到，再进行矫正倾斜
        else:
            # 矫正倾斜
            img_data = fourier_demo(img_flu)
            # 直接识别
            barcodes = pyzbar.decode(img_data)
            if len(barcodes) > 0:
                for barcode in barcodes:
                    # 提取条形码的边界框的位置
                    # 画出图像中条形码的边界框
                    (x, y, w, h) = barcode.rect
                    cv2.rectangle(img_data, (x - 50, y - 50), (x + w + 50, y + h + 50), (0, 255, 0), 2)
                    # 条形码数据为字节对象，所以如果我们想在输出图像上
                    # 画出来，就需要先将它转换成字符串
                    barcodeData = barcode.data.decode("utf-8")
                    barcodeType = barcode.type
                    # 参数（字体，默认大小）
                    font = ImageFont.truetype('msyh.ttc', 35)
                    # 字体颜色（rgb)
                    fillColor = (0, 255, 255)
                    # 文字输出位置
                    position = (x, y - 10)
                    # 输出内容
                    str = barcodeData
                    cv2.putText(img_data, str, position, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2)
                    # 向终端打印条形码数据和条形码类型
                    print("扫描结果==》 类别： {0} 内容： {1}".format(barcodeType, barcodeData))
                cv2.imwrite("result-" + img_url[0:-4] + ".png", img_data, params=None)
            # 如果识别不到，再进行定位裁剪
            else:
                cc = 3
                while cc < 12:
                    img, temp = pos(img_data, cc)
                    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    img = cv2.resize(img, None, fx=2, fy=2, interpolation=None)
                    cc = cc + 2
                    barcodes = Barcode(img)
                    for barcode in barcodes:
                        # 提取条形码的边界框的位置
                        # 画出图像中条形码的边界框
                        # (x, y, w, h) = barcode.rect
                        cv2.rectangle(img_data, (temp[0], temp[1]), (temp[2], temp[3]), (0, 255, 0), 2)
                        # 条形码数据为字节对象，所以如果我们想在输出图像上
                        # 画出来，就需要先将它转换成字符串
                        barcodeData = barcode.data.decode("utf-8")
                        barcodeType = barcode.type
                        # 参数（字体，默认大小）
                        font = ImageFont.truetype('msyh.ttc', 35)
                        # 字体颜色（rgb)
                        fillColor = (0, 255, 255)
                        # 文字输出位置
                        position = (temp[0], temp[1] - 10)
                        # 输出内容
                        str = barcodeData
                        # 需要先把输出的中文字符转换成Unicode编码形式(  str.decode("utf-8)   )
                        cv2.putText(img_data, str, position, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2)
                        # 向终端打印条形码数据和条形码类型
                        print("扫描结果==》 类别： {0} 内容： {1}".format(barcodeType, barcodeData))
                    if len(barcodes) > 0:
                        cv2.imwrite("result-" + img_url[0:-4] + ".png", img_data, params=None)
                        break
        img_url = input("请输入图片的路径")
